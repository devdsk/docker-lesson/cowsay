# Dockerfile. Пример приложения для создания образа

В этом проекте содержится артефакт приложения-примера для запуска внутри контейнера, созданного из Dockerfile. Также здесь можно найти краткое описание команд для
Dockerfile.

## Артефакт приложения

Скачать артефакт приложения вы можете по ссылке:

https://gitlab.com/devdsk/docker-lesson/docker-file/-/package_files/110615030/download

Или с помощью командной строки:

```bash
wget -O cowsay_2.0.4_Linux_x86_64.tar.gz https://gitlab.com/devdsk/docker-lesson/docker-file/-/package_files/110615030/download
```

```bash
curl -o cowsay_2.0.4_Linux_x86_64.tar.gz https://gitlab.com/devdsk/docker-lesson/docker-file/-/package_files/110615030/download
```

Исполняемый файл упакован в архив `tar.gz` и его нужно распаковать:

```bash
tar -xvzf cowsay_2.0.4_Linux_x86_64.tar.gz cowsay
```

## Запуск приложения

Запуск приложения с аргументом `Hello world` привидет к выводу следующего формата:

```bash
cowsay Hello world 
```

```bash
 _____________ 
< Hello world >
 ------------- 
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

## Практическая работа №2. Создание образа с помощью Dockerfile

1. Авторизоваться в registry.gitlab.com командой:

```bash
docker login registry.gitlab.com -u user -p gldt-Td_7H7oJdpQBx6Uw-PzF
```

2. Написать Dockerfile по следующим критериям:

 - Использовать образ “bash:latest” в качестве базового;
 - Создать файл id.txt с вашими инициалами и последним октетом ip-адреса;
 - При запуске контейнера приложение “cowsay” должно вывести в stdout содержимое id.txt. Например:

```bash
 _____________ 
< Кузьменко Д.С. 36 >
 ------------- 
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

Артефакт приложения можно скачать по ссылке: https://gitlab.com/devdsk/docker-lesson/docker-file/-/package_files/110615030/download

3. Собрать образ из Dockerfile и протегировать последним октетом ip адреса  с помощью команды:

```bash
docker build <путь к контексту сборки> -t registry.gitlab.com/devdsk/docker-lesson/docker-file/cow-id:<последний октет ip>
```

4. Отправить собраный образ в репозиторий с помощью команды:

```bash
docker push registry.gitlab.com/devdsk/docker-lesson/docker-file/cow-id:<последний октет ip>
```

### 
---
### 

# Краткое руководство по Dockerfile

Это краткое руководство по Dockerfile.

## Содержание

- [Краткое руководство по Dockerfile](#краткое-руководство-по-dockerfile)
  - [Содержание](#содержание)
  - [shell и exec форма](#shell-и-exec-форма)
  - [FROM](#from)
  - [RUN](#run)
  - [COPY](#copy)
  - [WORKDIR](#workdir)
  - [ENV](#env)
  - [VOLUME](#volume)
  - [ENTRYPOINT](#entrypoint)
  - [CMD](#cmd)
  - [CMD + ENTRYPOINT](#cmd-+-entrypoint)
  - [LABEL](#label)
  - [USER](#user)

### [shell и exec форма](https://docs.docker.com/engine/reference/builder/#shell-and-exec-form)

Команды **RUN**, **CMD**, **ENTRYPOINT** в Dockerfile могут быть переданы в `shell` или `exec` формате.
При использовани `shell` формата строка следующая непосредственно после команды будет обернута в `sh -c`.
Например команда:

```Dockerfile
RUN apt-get update && apt-get install -y
```

внутри контейнера будет выполнена следующим образом:

```bash
sh -c 'apt-get update && apt-get install -y'
```

При использовани `exec` формата строки переданные внутри списка будут выполнены следующим образом:

```Dockerfile
ENTRYPOINT ["python", "application.py"]
```

```bash
exec python application.py
```

### [FROM](https://docs.docker.com/engine/reference/builder/#from)

Инструкция **FROM** устанавливает базовый образ, на основе которого будет строится ваш Docker-образ. Пример:

```Dockerfile
FROM ubuntu:latest
```

### [RUN](https://docs.docker.com/engine/reference/builder/#run)

Инструкция **RUN** выполняет команды внутри контейнера во время сборки образа. Может быть указана как в `exec` так и `shell` формате. Пример:

```Dockerfile
RUN apt-get update && apt-get install -y
RUN ["apt-get", "update"]
```

### [COPY](https://docs.docker.com/engine/reference/builder/#copy)
[[наверх](#содержание)]

Инструкция **COPY** копирует файлы из исходного пути в контейнер. Пример:

```Dockerfile
COPY test.txt relativeDir/
COPY test.txt /absoluteDir/
```

### [WORKDIR](https://docs.docker.com/engine/reference/builder/#workdir) 
[[наверх](#содержание)]

Инструкция **WORKDIR** устанавливает текущий каталог для последующих команд. Пример:

```Dockerfile
WORKDIR /app
```

### [ENV](https://docs.docker.com/engine/reference/builder/#env) 
[[наверх](#содержание)]

Инструкция **ENV** устанавливает переменные окружения в контейнере. Пример:

```Dockerfile
ENV APP_PORT=1234
```

### [VOLUME](https://docs.docker.com/engine/reference/builder/#volume) 
[[наверх](#содержание)]

Инструкция **VOLUME** создает временный docker-volume и монтирует его по указанному пути. Пример:

```Dockerfile
VOLUME /data
```

### [ENTRYPOINT](https://docs.docker.com/engine/reference/builder/#entrypoint) 
[[наверх](#содержание)]

Инструкция **ENTRYPOINT** устанавливает точку входа для контейнера, команду которая будет выполнятся при запуске.
Таким образом контейнер может работать как исполняемый файл. Параметры, задаваемые в **ENTRYPOINT**, не перезаписываются в том случае, 
если контейнер запускают с параметрами командной строки. Например, после выполнения команды вида `docker run my_image bash` аргумент `bash` добавится в конец списка аргументов, заданных с помощью **ENTRYPOINT**. Может быть представлена как в `shell` так и в `exec` формате.
Пример:

```Dockerfile
ENTRYPOINT cowsay "Hello"
ENTRYPOINT ["cowsay", "Hello"]
```


### [CMD](https://docs.docker.com/engine/reference/builder/#cmd) 
[[наверх](#содержание)]

Инструкция **CMD** предоставляет Docker команду, которую нужно выполнить при запуске контейнера. Но в отличии от **ENTRYPOINT** ее можно переопределить аргументамипередаваемыми командой `docker run`. В одном файле Dockerfile может присутствовать лишь одна инструкция **CMD**. Если в файле есть несколько таких инструкций, система проигнорирует все кроме последней. Может быть представлена как в `shell` так и в `exec` формате.
Пример:

```Dockerfile
CMD ["cowsay", "Hello"]
CMD cowsay "Hello"
```

### [CMD + ENTRYPOINT](https://docs.docker.com/engine/reference/builder/#understand-how-cmd-and-entrypoint-interact)
[[наверх](#содержание)]

В Dockerfile допускается обьединение команд **CMD** и **ENTRYPOINT** в различных формах. Важно отметить, что в Dockerfile
обязательно должна присутствовать хотя бы ОДНА из команд **CMD** или **ENTRYPOINT**.
Результат обьединения команд **CMD** и **ENTRYPOINT** представлен в таблице

|                            | Без **ENTRYPOINT**  | **ENTRYPOINT** `cmd1 arg1` | **ENTRYPOINT** `["cmd1", "arg1"]`|
| :---                       |      :---           |     :---                   |         :---                     |
| Без **CMD**                | НЕ ПОДДЕРЖИВАЕТСЯ   | `sh -c 'cmd1 arg1'`        | `exec cmd1 arg1`                      |
| **CMD** `["cmd2", "arg2"]` | `exec cmd2 arg2`         | `sh -c 'cmd1 arg1'`        | `exec cmd1 arg1 cmd2 arg2`            |
| **CMD**  `cmd2 arg2`       | `sh -c 'cmd2 arg2'` | `sh -c 'cmd1 arg1'`        | `exec cmd1 arg1 sh -c 'cmd2 arg2'`    |

Например:

```Dockerfile
ENTRYPOINT ["cowsay"]
CMD ["Hello"]
```

В этом случае внутри контейнера при запуске будет выполнена команда `cowsay Hello`.

### [LABEL](https://docs.docker.com/engine/reference/builder/#label) 
[[наверх](#содержание)]

Инструкция **LABEL** добавляет метаданные к образу. Пример:

```Dockerfile
LABEL version="1.0" maintainer="Soul Eater"
```

## [USER](https://docs.docker.com/engine/reference/builder/#user) 
[[наверх](#содержание)]

Инструкция **USER** устанавливает пользователя, от имени которого будут выполняются все последующие команды. 
Пример:

```Dockerfile
USER postgres
RUN pg_restore -d database --clean database_dump.tar
```